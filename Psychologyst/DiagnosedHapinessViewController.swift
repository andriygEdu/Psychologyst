//
//  DiagnosedHapinessViewControlles.swift
//  Psychologyst
//
//  Created by Andriy Gushuley on 23/08/15.
//  Copyright © 2015 andriyg. All rights reserved.
//

import UIKit

class DiagnosedHapinessViewController: HapinessViewController{
    private struct Story {
        static let SEGUEY_ID = "Show history"
    }
    override var happines: CGFloat {
        didSet {
            history?.addItem(happines)
        }
    }
    @IBOutlet var history: DiagnosysHistory?
    
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        if let id = segue.identifier,
            let tvc = (segue.destination as? TextViewController)
        {
            switch id {
            case Story.SEGUEY_ID:
                if let popover = tvc.popoverPresentationController {
                    popover.permittedArrowDirections = .any
                    popover.delegate = tvc
                    
                }
                tvc.text = history?.text ?? ""
            default:
                break;
            }
        }
   }
}
