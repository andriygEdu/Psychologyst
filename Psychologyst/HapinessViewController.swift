//
//  HapinessViewController.swift
//  HapinessII
//
//  Created by Andriy Gushuley on 18/08/15.
//  Copyright © 2015 andriyg. All rights reserved.
//

import UIKit

class HapinessViewController: UIViewController,
    FaceViewDataSource
{
    
    var happines: CGFloat = 0.25 {
        didSet {
            happines = min(max(happines, -1), 1)
            print("Heppines: \(happines)")
            updateUi()
        }
    }
    var confusion: CGFloat = 0.0 {
        didSet {
            confusion = min(max(confusion, -1), 1)
            print("Confusion: \(happines)")
            updateUi()
        }
    }
    
    @IBOutlet weak var faceView: FaceView? {
        didSet {
            faceView?.data = self
            
            if (pinchRecognizer != nil) {
                faceView?.addGestureRecognizer(pinchRecognizer!)
            }
            if (panRecognizer != nil) {
                faceView?.addGestureRecognizer(panRecognizer!)
            }
        }
    }
    
    func updateUi() {
        faceView?.setNeedsDisplay()
    }
    
    @IBAction func pinch(_ recognizer: UIPinchGestureRecognizer) {
        if ( recognizer.state == .changed
            || recognizer.state == .ended )
        {
            faceView?.scale *= recognizer.scale
            recognizer.scale = 1;
        }
    }
    
    @IBAction func pan(_ recognizer: UIPanGestureRecognizer) {
        if ( recognizer.state == .changed
            || recognizer.state == .ended )
        {
            if (faceView != nil) {
                let translation = recognizer.translation(in: faceView!)
                
                happines += translation.y / 100.0
                confusion += -translation.x / 100.0
                
                recognizer.setTranslation(CGPoint.zero, in: faceView!)
            }
        }
        
    }
    
    @IBOutlet weak var panRecognizer: UIPanGestureRecognizer? {
        didSet {
            faceView?.addGestureRecognizer(panRecognizer!)
        }
    }
    
    @IBOutlet weak var pinchRecognizer: UIPinchGestureRecognizer? {
        didSet {
            faceView?.addGestureRecognizer(pinchRecognizer!)
        }
    }
}
