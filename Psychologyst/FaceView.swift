//
//  FaceView.swift
//  HapinessII
//
//  Created by Andriy Gushuley on 18/08/15.
//  Copyright © 2015 andriyg. All rights reserved.
//

import UIKit

let DEFAULT_FACE_SCALE: CGFloat = 0.95
let EYE_Y_DISP: CGFloat = 0.35
let EYE_X_DISP: CGFloat = 0.35
let EYE_RADIUS: CGFloat = 0.10
let MOUTH_Y_DISP: CGFloat = 0.35
let MOUTH_HALF_WITH: CGFloat = 0.50
let MOUNT_MAX_DOWN: CGFloat = 0.30
let CONFUSION_MAX_DISP: CGFloat = 0.50

let TWO_PI = CGFloat(2 * M_PI)

@objc
protocol FaceViewDataSource {
    var happines: CGFloat {
        get
    }
    var confusion: CGFloat {
        get
    }
}

@IBDesignable
class FaceView: UIView {
    @IBInspectable
    var lineWith:CGFloat = 3 { didSet { setNeedsDisplay() } }
    @IBInspectable
    var color: UIColor = UIColor.blue { didSet { setNeedsDisplay() } }
    @IBInspectable
    var scale: CGFloat = DEFAULT_FACE_SCALE { didSet {
        scale = max(-1, min(1, scale))
        setNeedsDisplay()
        } }
    weak var data: FaceViewDataSource?
    
    private var faceCenter: CGPoint {
        return convert(center, from: superview)
    }
    
    private var faceRadius: CGFloat {
        return min(bounds.size.width, bounds.size.height) * scale / 2
    }
    
    private enum Eye {
        case left
        case right
    }
    
    private func eyePath(_ eye: Eye) -> UIBezierPath {
        let eyeRadius = faceRadius * EYE_RADIUS
        let eyeVerticalOffset = faceRadius * EYE_Y_DISP
        let eyeHorizontalOffset = faceRadius * EYE_X_DISP
        
        var eyeCenter = faceCenter
        eyeCenter.y -= eyeVerticalOffset
        switch eye {
        case .left: eyeCenter.x -= eyeHorizontalOffset
        case .right: eyeCenter.x += eyeHorizontalOffset
        }
        
        let path = UIBezierPath(arcCenter: eyeCenter, radius: eyeRadius, startAngle: 0, endAngle: TWO_PI, clockwise: true)
        path.lineWidth = lineWith
        return path
    }
    
    private func mouthPath(_ _happiness: CGFloat, confusion _confusion: CGFloat) -> UIBezierPath {
        let hapiness  = min(1, max(-1, _happiness))
        let confusion = min(1, max(-1, _confusion))
        
        let moutWith = faceRadius * MOUTH_HALF_WITH * 2
        let mouthHeigh = faceRadius * MOUNT_MAX_DOWN * hapiness
        let mouthVertialOffset = faceRadius * MOUTH_Y_DISP
        
        let start = CGPoint(x: faceCenter.x - moutWith / 2, y: faceCenter.y + mouthVertialOffset)
        let end = CGPoint(x: start.x + moutWith, y: start.y)
        
        let confusionDisp = confusion * moutWith * CONFUSION_MAX_DISP
        let rightMiddleX = start.x + (moutWith * 2 / 3) - confusionDisp / 2
        let cp1 = CGPoint(x: start.x + moutWith / 3 - confusionDisp / 2, y: start.y + mouthHeigh)
        let cp2 = CGPoint(x: rightMiddleX, y: start.y + mouthHeigh)
        
        let path = UIBezierPath()
        path.move(to: start)
        path.addCurve(to: end, controlPoint1: cp1, controlPoint2: cp2)
        path.lineWidth = lineWith
        
        return path
    }
    
    override func draw(_ rect: CGRect) {
        let oval = UIBezierPath(arcCenter: faceCenter, radius: faceRadius,
            startAngle: 0, endAngle: CGFloat(2 * M_PI), clockwise: true)
        
        oval.lineWidth = lineWith
        color.set()
        oval.stroke()
        
        eyePath(.left).stroke()
        eyePath(.right).stroke()
        
        mouthPath(data?.happines ?? 0, confusion: data?.confusion ?? 0).stroke()
    }
    
    
}
