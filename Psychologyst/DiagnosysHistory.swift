//
//  DisgnosysHistory.swift
//  Psychologyst
//
//  Created by Andriy Gushuley on 27/08/15.
//  Copyright © 2015 andriyg. All rights reserved.
//

import UIKit

class DiagnosysHistory: NSObject {
    private let defaults = UserDefaults.standard
    var history: [CGFloat] {
        get {
            return defaults.object(forKey: "DiagnosysHistory") as? [CGFloat] ?? []
        }
        set {
            defaults.set(newValue, forKey: "DiagnosysHistory")
        }
    }
    func addItem(_ value: CGFloat) {
        history += [value]
    }
    
    var text: String {
        return "\(history)"
    }
}
