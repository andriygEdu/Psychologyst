//
//  TextViewController.swift
//  Psychologyst
//
//  Created by Andriy Gushuley on 23/08/15.
//  Copyright © 2015 andriyg. All rights reserved.
//

import UIKit

class TextViewController: UIViewController, UIPopoverPresentationControllerDelegate {
    @IBOutlet weak var textView: UITextView? {
        didSet {
            textView?.text = text
        }
    }
    
    var text: String = "" {
        didSet {
            textView?.text = text
        }
    }
    
    override var preferredContentSize: CGSize {
        get {
            if let textView = textView {
                return textView.contentSize
            }
            return super.preferredContentSize
        }
        set {
            super.preferredContentSize = newValue
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }

}
