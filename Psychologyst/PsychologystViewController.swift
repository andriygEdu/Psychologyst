//
//  ViewController.swift
//  Psychologyst
//
//  Created by Andriy Gushuley on 23/08/15.
//  Copyright © 2015 andriyg. All rights reserved.
//

import UIKit

class PsychologystViewController: UIViewController {

    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        if let hvc = (segue.destination as? UINavigationController)?.visibleViewController as? HapinessViewController,
            let id = segue.identifier
        {
            hvc.confusion = 0
            switch id {
            case "sad":
                hvc.happines = -1
            case "happy":
                hvc.happines = 1
            case "nothing":
                hvc.happines = 1 / 3
            
            default:
                hvc.happines = 0
            }
        }
    }

    @IBAction func nothingSelected(_ sender: UIButton) {
        performSegue(withIdentifier: "nothing", sender: self)
    }
}

